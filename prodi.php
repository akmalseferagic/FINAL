<?php
include_once 'top.php';
require_once 'db/class_prodi.php';
?>
<h2>Daftar Program Studi</h2>
<div class="panel-header textMargin">
    <a class="btn icon-btn btn-success" href="form_prodi.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Program Studi
    </a>
</div>
<?php
$obj = new Prodi();
$rows = $obj->getAll();
?>

<script language="javascript">
        $(document).ready(function() {
        $('#example').DataTable();
        });
</script>

<table id="example" class="table table-striped table-bordered">
    <thead>
    <tr class="active">
	<th>ID</th>
	<th>Kode</th>
	<th>Nama</th>
	<th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['kode'].'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td><a href="view_prodi.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_prodi.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
    <div align=center>
        <a href="grafik_prodi.php" class="btn btn-info" role="button">Lihat Grafik Program Studi</a>
    </div>
  
<?php
include_once 'bottom.php';
?>
