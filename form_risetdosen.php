<?php

/*
mysql> desc riset_dosen;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| dosen_id         | int(11)      | NO   | MUL | NULL    |                |
| judul            | text         | YES  |     | NULL    |                |
| sumber_pendanaan | varchar(45)  | YES  |     | NULL    |                |
| biaya            | double       | YES  |     | NULL    |                |
| mulai_semester   | int(11)      | YES  |     | NULL    |                |
| akhir_semester   | int(11)      | YES  |     | NULL    |                |
| deskripsi        | varchar(100) | YES  |     | NULL    |                |
| kategori_id      | int(11)      | NO   | MUL | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+
9 rows in set (0,03 sec)


    */

    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_risetd.php';
    //buat variabel untuk memanggil class
    $obj_kegiatan = new Risetd();
    //buat variabel utk menyimpan id
    $_idedit = @$_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_riset.js"></script>
<form name="form_riset" class="form-horizontal" method="POST" action="proses_risetdosen.php">
<fieldset>

<!-- Form Name -->
<legend>Form Riset Dosen</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">ID Dosen</label>  
  <div class="col-md-4">
  <input id="dosen_id" name="dosen_id" type="text" placeholder="Masukkan id Dosen" class="form-control input-md" value="<?php echo isset($data['dosen_id']) ? $data['dosen_id']: '' ?>">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Judul</label>  
  <div class="col-md-4">
  <input id="judul" name="judul" type="text" placeholder="Masukkan Judul" class="form-control input-md" value="<?php echo isset($data['judul']) ? $data['judul']: '' ?>">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Sumber Pendanaan</label>  
  <div class="col-md-4">
  <input id="sumber_pendanaan" name="sumber_pendanaan" type="text" placeholder="Masukkan Sumber" class="form-control input-md" value="<?php echo isset($data['sumber_pendanaan']) ? $data['sumber_pendanaan']: '' ?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Biaya</label>  
  <div class="col-md-4">
  <input id="biaya" name="biaya" type="text" placeholder="Masukkan biaya" class="form-control input-md" value="<?php echo isset($data['biaya']) ? $data['biaya']: '' ?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Mulai di Semester</label>  
  <div class="col-md-4">
  <input id="mulai_semester" name="mulai_semester" type="text" placeholder=" " class="form-control input-md" value="<?php echo isset($data['mulai_semester']) ? $data['mulai_semester']: '' ?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Berakhir di Semester</label>  
  <div class="col-md-4">
  <input id="akhir_semester" name="akhir_semester" type="text" placeholder=" " class="form-control input-md" value="<?php echo isset($data['akhir_semester']) ? $data['akhir_semester']: '' ?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Deskripsi Riset</label>  
  <div class="col-md-4">
  <input id="deskripsi" name="deskripsi" type="text" placeholder="Tulis Deskripsi" class="form-control input-md" value="<?php echo isset($data['deskripsi']) ? $data['deskripsi']: '' ?>">
    
  </div>
</div>


<!-- Text input-->

<div class="form-group">
      <label class="col-md-4 control-label" for="prodi_id">ID Kategori</label>
      <div class="col-md-4">
        <select id="kategori_id" name="kategori_id" class="form-control">
          <option value="0">Pilih ID Kategori</option>
          <option value="1">1. 1</option>
          <option value="2">2. 2</option>
          <option value="2">2. 3</option>
        </select>
      </div>
    </div>




<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>