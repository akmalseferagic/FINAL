<?php
/*mysql> desc riset_dosen;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| dosen_id         | int(11)      | NO   | MUL | NULL    |                |
| judul            | text         | YES  |     | NULL    |                |
| sumber_pendanaan | varchar(45)  | YES  |     | NULL    |                |
| biaya            | double       | YES  |     | NULL    |                |
| mulai_semester   | int(11)      | YES  |     | NULL    |                |
| akhir_semester   | int(11)      | YES  |     | NULL    |                |
| deskripsi        | varchar(100) | YES  |     | NULL    |                |
| kategori_id      | int(11)      | NO   | MUL | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+
9 rows in set (0,03 sec)


    */

    include_once 'top.php';
    require_once 'db/class_risetd.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objKegiatan = new Risetd();
    $_id = $_GET['id'];
    $data = $objKegiatan->findByID($_id);
?>
<!--Buat tampilan dengan tabel-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Kegiatan</h3>
            </div>
            <div class="panel-body">
                <table class="table">
                <tr>
                <td class="active">id Dosen</td><td>:</td><td><?php echo
                $data['dosen_id']?></td>
                </tr>
                <tr>
                <td class="active">Judul</td><td>:</td><td><?php echo
                $data['judul']?></td>
                </tr>
                <tr>
                <td class="active">Sumber Pendanaan</td><td>:</td><td><?php echo
                $data['sumber_pendanaan']?></td>
                </tr>
                <tr>
                <td class="active">Biaya</td><td>:</td><td><?php echo
                $data['biaya']?></td>
                <tr>
                <td class="active">Mulai Semester</td><td>:</td><td><?php echo
                $data['mulai_semester']?></td>
                <tr>
                <td class="active">Akhir Semester</td><td>:</td><td><?php echo
                $data['akhir_semester']?></td>
                <tr>
                <td class="active">Deskripsi</td><td>:</td><td><?php echo
                $data['deskripsi']?></td>
                <tr>
                <td class="active">id Kategori</td><td>:</td><td><?php echo
                $data['kategori_id']?></td>
                </tr>
                </tr>
                </tr>
                </tr>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include_once 'bottom.php';
?>