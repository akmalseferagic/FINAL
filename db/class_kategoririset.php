<?php
    /*
    mysql> desc kategori_riset;
+-------+-------------+------+-----+---------+----------------+
| Field | Type        | Null | Key | Default | Extra          |
+-------+-------------+------+-----+---------+----------------+
| id    | int(11)     | NO   | PRI | NULL    | auto_increment |
| nama  | varchar(45) | YES  |     | NULL    |                |
+-------+-------------+------+-----+---------+----------------+
2 rows in set (0,21 sec)

    */

    require_once "DAO.php";
    class Kategori extends DAO
    {
        public function __construct()
        {
            parent::__construct("kategori_riset");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,nama) ".
            " VALUES (default,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET nama=? ".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik

    }
?>