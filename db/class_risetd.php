<?php
    /*
mysql> select * from riset_dosen;
+----+----------+----------------------------------------------------------+------------------+-----------+----------------+----------------+----------------------------+-------------+
| id | dosen_id | judul                                                    | sumber_pendanaan | biaya     | mulai_semester | akhir_semester | deskripsi                  | kategori_id |
+----+----------+----------------------------------------------------------+------------------+-----------+----------------+----------------+----------------------------+-------------+
|  1 |       15 | Pengembangan Framework Mobile Aplication                 | Dikti            |  21000000 |          20172 |          20181 | pengajuan hibah dikti 2018 |           1 |
|  2 |       20 | Pengembangan Alat Deteksi Janin Bayi                     | Dikti            | 221000000 |          20172 |          20191 | pengajuan hibah dikti 2018 |           3 |
|  3 |       16 | Analisa Peran Social Media Pemilihan Gubernur Jabar 2018 | Dikti            |  11000000 |          20172 |          20181 | pengajuan hibah dikti 2018 |           2 |
+----+----------+----------------------------------------------------------+------------------+-----------+----------------+----------------+----------------------------+-------------+
3 rows in set (0,00 sec)

mysql> desc riset_dosen;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| dosen_id         | int(11)      | NO   | MUL | NULL    |                |
| judul            | text         | YES  |     | NULL    |                |
| sumber_pendanaan | varchar(45)  | YES  |     | NULL    |                |
| biaya            | double       | YES  |     | NULL    |                |
| mulai_semester   | int(11)      | YES  |     | NULL    |                |
| akhir_semester   | int(11)      | YES  |     | NULL    |                |
| deskripsi        | varchar(100) | YES  |     | NULL    |                |
| kategori_id      | int(11)      | NO   | MUL | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+
9 rows in set (0,03 sec)


    */

    require_once "DAO.php";
    class Risetd extends DAO
    {
        public function __construct()
        {
            parent::__construct("riset_dosen");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,dosen_id,judul,sumber_pendanaan,biaya,mulai_semester,akhir_semester,deskripsi,kategori_id) ".
            " VALUES (default,?,?,?,?,?,?,?,?)";
            
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET dosen_id=?,judul=?,sumber_pendanaan=?,biaya=?,mulai_semester=?,akhir_semester=?,deskripsi=?,kategori_id=? ".
            " WHERE id=?";

            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik

    }
?>