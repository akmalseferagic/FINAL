<?php
    require_once "DAO.php";
    class Dosen extends DAO
    {
        public function __construct()
        {
            parent::__construct("dosen");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,nidn,nama,gelar_depan,gelar_belakang,tmp_lahir,jk,prodi_id,email,jabatan_id,pend_akhir) ".
            " VALUES (default,?,?,?,?,?,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET nidn=?,nama=?,gelar_depan=?,gelar_belakang=?,tmp_lahir=?,jk=?,prodi_id=?,email=?,jabatan_id=?,pend_akhir=? ".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        
	public function getStatistik() {
		$sql = "select a.nama,count(b.id) as jumlah from prodi a LEFT JOIN dosen b on a.id=b.prodi_id group by a.nama";
		$ps = $this->koneksi->prepare($sql);
		$ps->execute();
		return $ps->fetchAll();
	}

    }
?>
