<?php
    require_once "DAO.php";
    class Prodi extends DAO
    {
        public function __construct()
        {
            parent::__construct("prodi");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,kode,nama) ".
            " VALUES (default,?,?)";

            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            	return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET kode=?,nama=?".
            " WHERE id=?";
            
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            	return $ps->rowCount();
        }


        
	public function getStatistik() {
		$sql = "select a.nama,count(b.id) as jumlah from prodi a LEFT JOIN dosen b on a.id=b.prodi_id group by a.nama";
		$ps = $this->koneksi->prepare($sql);
		$ps->execute();
		return $ps->fetchAll();
	}

    }
?>
