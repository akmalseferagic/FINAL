<?php
    require_once "DAO.php";
    class Rombel extends DAO
    {
        public function __construct()
        {
            parent::__construct("rombel");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,nama,mhs_angkatan,dosen_pa,prodi_id) ".
            " VALUES (default,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET nama=?,mhs_angkatan=?,dosen_pa=?,prodi_id=? ".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        
	public function getStatistik() {
		$sql = "select a.nama,count(b.id) as jumlah from rombel a LEFT JOIN dosen b on a.id=b.prodi_id group by a.nama";
		$ps = $this->koneksi->prepare($sql);
		$ps->execute();
		return $ps->fetchAll();
	}

    }
?>
