<!DOCTYPE html>
<html lang="en">
    <head>
        <title> AkRa 47 </title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        
        <link href="https://fonts.googleapis.com/css?family=Arsenal" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <style>
            body { 
                background-color: #e74c3c;
                font-family: 'Open Sans', sans-serif;
                color: aliceblue;
            }
            h1 { margin-top: 150px;}
            .marginLeft2 {margin-left: 55px;}
            .marginLeft1 {margin-left: 20px;}
            .marginBottom {margin-bottom: 25px;}
        </style>
    </head>

<body>
    
    <div class="container content " >
        <div class="row">
            
            <div class="col-md-12" >
                <div align=center>
                    <h1>hallo</h1>
                    <h2>Selamat Datang di AkRa 47</h2>
                    <h2 class="marginBottom">Silahkan pilih tabel yang ingin anda Lihat</h2>
                </div>
            </div>
            <div class="col-md-4" >
                    
            </div>
        </div>
    </div>
    
    
    <div class="container content " >
        <div class="row">
            <div class="col-md-3" >
                
            </div>
            <div class="col-md-3 marginLeft1" >
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" >Master Dosen
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="dosen.php">Identitas Dosen</a></li>
                        <li><a href="prodi.php">Program Studi</a></li>
                        <li><a href="rombel.php">Rombel</a></li>
                    </ul>
                </div> 
            </div>
            <div class="col-md-3 marginLeft2" >
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown" >Riset Dosen
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="kategoririset.php">Kategori Riset</a></li>
                        <li><a href="risetdosen.php">Riset Dosen</a></li>
                    </ul>
                </div> 
            </div>
            <div class="col-md-3" >
                
            </div>
        </div>
    </div>
    
    
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<!-- Latest compiled and minified JavaScript --> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
