<?php

/*mysql> desc riset_dosen;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| dosen_id         | int(11)      | NO   | MUL | NULL    |                |
| judul            | text         | YES  |     | NULL    |                |
| sumber_pendanaan | varchar(45)  | YES  |     | NULL    |                |
| biaya            | double       | YES  |     | NULL    |                |
| mulai_semester   | int(11)      | YES  |     | NULL    |                |
| akhir_semester   | int(11)      | YES  |     | NULL    |                |
| deskripsi        | varchar(100) | YES  |     | NULL    |                |
| kategori_id      | int(11)      | NO   | MUL | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+
9 rows in set (0,03 sec)


    */

include_once 'top.php';
require_once 'db/class_risetd.php';
?>
<h2>Daftar Riset Dosen</h2>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_risetdosen.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Riset Dosen
    </a>
</div>
<?php
$obj = new Risetd();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>
<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr class="active">
        <th>No</th>
        <th>ID Dosen</th>
        <th>Judul</th>
        <th>Sumber Pendanaan</th>
        <th>Biaya</th>
        <th>Mulai Semester</th>
        <th>Akhir Semester</th>
        <th>Deskripsi</th>
        <th>ID Kategori</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['dosen_id'].'</td>';
        echo '<td>'.$row['judul'].'</td>';
        echo '<td>'.$row['sumber_pendanaan'].'</td>';
        echo '<td>'.$row['biaya'].'</td>';
        echo '<td>'.$row['mulai_semester'].'</td>';
        echo '<td>'.$row['akhir_semester'].'</td>';
        echo '<td>'.$row['deskripsi'].'</td>';
        echo '<td>'.$row['kategori_id'].'</td>';
        echo '<td><a href="view_risetdosen.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_risetdosen.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>

<?php
include_once 'bottom.php';
?>