<?php
    include_once 'top.php';
    require_once 'db/class_rombel.php';
    $objKegiatan = new Rombel();
    $_id = $_GET['id'];
    $data = $objKegiatan->findByID($_id);
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Rombel</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered">
                    <tr>
                        <td class="active">Nama</td>
                        <td>:</td><td>
                        <?php echo $data['nama']?></td>
                    </tr>
                    <tr>
                        <td class="active">Mahasiswa Angkatan</td>
                        <td>:</td>
                        <td><?php echo $data['mhs_angkatan']?></td>
                    </tr>
                    <tr>
                        <td class="active">Dosen Pembimbing</td>
                        <td>:</td>
                        <td><?php echo $data['dosen_pa']?></td>
                    </tr>
                    <tr>
                        <td class="active">Program Studi ID</td>
                        <td>:</td>
                        <td><?php echo $data['prodi_id']?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
    include_once 'bottom.php';
?>
