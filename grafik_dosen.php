<?php
	include_once 'top.php';
	require_once 'db/class_dosen.php';

    $obj = new Dosen();
    $rs = $obj->getStatistik();
    $ar_data = [];
    foreach($rs as $row){
        $ar['label']=$row['nama'];
        $ar['y']=(int)$row['jumlah'];
        $ar_data[]=$ar;
    }
    $out = array_values($ar_data);
?>

<script type="text/javascript">
window.onload = function() {
	
	var chart = new CanvasJS.Chart("chartContainer", {
		theme: "light1", // "light2", "dark1", "dark2"
		animationEnabled: false, // change to true
		title:{
			text: "Chart Dosen"
		},
		data: [
		{
			type: "column",
			dataPoints:<?php echo json_encode($out) ?>
		}
		]
	});
chart.render();
}
</script>
<body>

<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js">
</script>

<?php
    include_once 'bottom.php'
?>
