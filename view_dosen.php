<?php
    include_once 'top.php';
    require_once 'db/class_dosen.php';
    $objKegiatan = new Dosen();
    $_id = $_GET['id'];
    $data = $objKegiatan->findByID($_id);
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Dosen</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered">
                    <tr>
                        <td class="active">nidn</td>
                        <td>:</td><td>
                        <?php echo $data['nidn']?></td>
                    </tr>
                    <tr>
                        <td class="active">nama</td>
                        <td>:</td>
                        <td><?php echo $data['nama']?></td>
                    </tr>
                    <tr>
                        <td class="active">Gelar Depan</td>
                        <td>:</td>
                        <td><?php echo $data['gelar_depan']?></td>
                    </tr>
                    <tr>
                        <td class="active">Gelar Belakang</td>
                        <td>:</td>
                        <td><?php echo $data['gelar_belakang']?></td>
                    </tr>
                    <tr>
                        <td class="active">Tempat Lahir</td>
                        <td>:</td>
                        <td><?php echo $data['tmp_lahir']?></td>
                    </tr>
                    <tr>
                        <td class="active">Tanggal Lahir</td>
                        <td>:</td>
                        <td><?php echo $data['tgl_lahir']?></td>
                    </tr>
                    <tr>
                        <td class="active">Jenis Kelamin</td>
                        <td>:</td>
                        <td><?php echo $data['jk']?></td>
                    </tr>
                    <tr>
                        <td class="active">Prodi Id</td>
                        <td>:</td>
                        <td><?php echo $data['prodi_id']?></td>
                    </tr>
                    <tr>
                        <td class="active">Email</td>
                        <td>:</td>
                        <td><?php echo $data['email']?></td>
                    </tr>
                    <tr>
                        <td class="active">Jabatan Id</td>
                        <td>:</td>
                        <td><?php echo $data['jabatan_id']?></td>
                    </tr>
                    <tr>
                        <td class="active">Pendidikan Akhir</td>
                        <td>:</td>
                        <td><?php echo $data['pend_akhir']?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
    include_once 'bottom.php';
?>
