<?php
include_once 'top.php';
require_once 'db/class_rombel.php';
?>
<h2>Daftar Rombel</h2>
<div class="panel-header textMargin">
    <a class="btn icon-btn btn-success" href="form_rombel.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Rombel
    </a>
</div>
<?php
$obj = new Rombel();
$rows = $obj->getAll();
?>

<script language="javascript">
        $(document).ready(function() {
        $('#example').DataTable();
        });
</script>

<table id="example" class="table table-striped table-bordered">
    <thead>
    <tr class="active">
	<th>ID</th>
	<th>Nama</th>
	<th>Mahasiswa Angkatan</th>
    <th>Dosen Pembimbing Akademik</th>
    <th>Program Studi ID</th>
	<th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td>'.$row['mhs_angkatan'].'</td>';
        echo '<td>'.$row['dosen_pa'].'</td>';
        echo '<td>'.$row['prodi_id'].'</td>';
        echo '<td><a href="view_rombel.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_rombel.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
    <div align=center>
        <a href="grafik_rombel.php" class="btn btn-info" role="button">Lihat Grafik Rombel</a>
    </div>
  
<?php
include_once 'bottom.php';
?>
