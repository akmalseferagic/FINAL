<?php
include_once 'top.php';
require_once 'db/class_dosen.php';
?>
<h2>Daftar Identitas Dosen</h2>
<div class="panel-header textMargin">
    <a class="btn icon-btn btn-success" href="form_dosen.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Dosen
    </a>
</div>
<?php
$obj = new Dosen();
$rows = $obj->getAll();
?>

<script language="javascript">
        $(document).ready(function() {
        $('#example').DataTable();
        });
</script>

<table id="example" class="table table-striped table-bordered">
    <thead>
    <tr class="active">
        <th>No</th>
	<th>NIDN</th>
	<th>Nama</th>
	<th>Gelar Depan</th>
	<th>Gelar Belakang</th>
	<th>Tempat Lahir</th>
	<th>Tanggal Lahir</th>
	<th>Jenis Kelamin</th>
	<th>Prodi Id</th>
	<th>E-Mail</th>
	<th>Jabatan Id</th>
	<th>Pendidikan Akhir</th>
	<th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nidn'].'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td>'.$row['gelar_depan'].'</td>';
        echo '<td>'.$row['gelar_belakang'].'</td>';
        echo '<td>'.$row['tmp_lahir'].'</td>';
        echo '<td>'.$row['tgl_lahir'].'</td>';
        echo '<td>'.$row['jk'].'</td>';
        echo '<td>'.$row['prodi_id'].'</td>';
        echo '<td>'.$row['email'].'</td>';
        echo '<td>'.$row['jabatan_id'].'</td>';
        echo '<td>'.$row['pend_akhir'].'</td>';
        echo '<td><a href="view_dosen.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_dosen.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
    <div align=center>
        <a href="grafik_dosen.php" class="btn btn-info" role="button">Lihat Grafik Dosen</a>
    </div>
  
<?php
include_once 'bottom.php';
?>
