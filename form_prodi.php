<?php
    include_once 'top.php';
    require_once 'db/class_prodi.php';
    $obj_kegiatan = new Prodi();
    $_idedit = $_GET['id'];
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>

<script src="js/form_validasi_prodi.js"></script>

<form class="form-horizontal" method="POST" name="form_prodi" action="proses_prodi.php">
<fieldset>

<legend>Form Input Program Studi</legend>

<div class="marginTop">

    <div class="form-group">
      <label class="col-md-4 control-label" for="kode"> Kode Program Studi</label>
      <div class="col-md-4">
        <select id="kode" name="kode" class="form-control">
          <option value="0">Pilih Kode Program Studi Anda</option>
          <option value="SI">1. (SI) Sistem Informasi</option>
          <option value="TI">2. (TI) Teknik Informatika</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="nama">Nama</label>  
      <div class="col-md-4">
      <input id="nama" name="nama" type="text" placeholder="Masukkan Nama" class="form-control input-md" value="<?php echo $data['nama']?>" >  
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="proses"></label>
      <div class="col-md-8">
      <?php
        if(empty($_idedit)){
        ?>
          <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
        <?php
        }else{
          ?>
          <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
          <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
          <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
        <?php
        }?>
      </div>
    </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
