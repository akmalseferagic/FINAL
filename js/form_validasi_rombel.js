$(function(){

	$("form[name='form_rombel']").validate({
		rules: {
			nama:{
				required:true,
				maxlenght:45,
			},
			mhs_angkatan:"required",
			dosen_pa:"required",
            prodi_id:"required", 
		},
		messages:{
			nama:{
				required:"Tidak Boleh kosong",
				maxlenght:"Maximum 45 karakter",
			},
			mhs_angkatan:"Tidak Boleh kosong",
			dosen_pa:"Tidak Boleh kosong",
            prodi_id:"Tidak Boleh kosong",
		},
		submitHandler : function(form){
			form.submit();
		}
	});
});

