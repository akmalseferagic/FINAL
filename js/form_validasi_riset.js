//buat fungsi
$(function(){
    //panggil nama form lalu gunakan fungsi validasi
    $("form[name='form_riset']").validate({

        //buat aturan untuk input form
        rules:{
            dosen_id:{
                required:true,
                maxLength:10,
            },
            judul:"required",
            sumber_pendanaan:"required",
            biaya:"required",
            mulai_semsester:"required",
            akhir_semester:"required",
            deskripsi:"required",
            kategori_id:"required"
        },
        //tampilkan pesan
        messages:{
            dosen_id:{
                required:"Kode Wajib diisi!!",
                maxLength:"Max 10 Character",
            },
            judul:"Judul wajib diisi!!",
            sumber_pendanaan:"Sumber wajib diisi",
            biaya:"wajib isi !",
            mulai_semsester:"Wajib isi!",
            akhir_semester:"isi!",
            deskripsi:"Deskripsi wajib diisi",
            kategori_id:"isi"
        },
        //submit form
        submitHandler:function(form){
            form.submit();
        }
    });

});


