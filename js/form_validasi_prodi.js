$(function(){

	$("form[name='form_prodi']").validate({
		rules: {
			nama:{
				required:true,
				maxlenght:11,
			},
			kode:"required",            
		},
		messages:{
			nama:{
				required:"Tidak Boleh kosong",
				maxlenght:"Maximum 3 angka",
			},
			kode:"Tidak Boleh kosong",
		},
		submitHandler : function(form){
			form.submit();
		}
	});
});

