$(function(){

	$("form[name='form_dosen']").validate({
		rules: {
			nidn:{
				required:true,
				maxlenght:10,
			},
			nama:"required",
			gelar_belakang:"required",
			jk:"required",
			prodi_id:"required",
			jabatan_id:"required",
			pend_akhir:"required",
		},
		messages:{
			nidn:{
				required:"Tidak Boleh kosong",
				maxlenght:"Maximum 10 angka",
			},
			nama:"Tidak Boleh kosong",
			gelar_belakang:"Tidak Boleh kosong",
			jk:"Tidak Boleh kosong",
			prodi_id:"Tidak Boleh kosong",
			jabatan_id:"Tidak Boleh kosong",
			pend_akhir:"Tidak Boleh kosong",
		},
		submitHandler : function(form){
			form.submit();
		}
	});
});

