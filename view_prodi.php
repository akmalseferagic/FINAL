<?php
    include_once 'top.php';
    require_once 'db/class_prodi.php';
    $objKegiatan = new Prodi();
    $_id = $_GET['id'];
    $data = $objKegiatan->findByID($_id);
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Prodi</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered">
                    <tr>
                        <td class="active">Kode</td>
                        <td>:</td>
                        <td><?php echo $data['kode']?></td>
                    </tr>
                    <tr>
                        <td class="active">Nama</td>
                        <td>:</td>
                        <td><?php echo $data['nama']?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
    include_once 'bottom.php';
?>
