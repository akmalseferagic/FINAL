<?php
/*mysql> desc riset_dosen;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| dosen_id         | int(11)      | NO   | MUL | NULL    |                |
| judul            | text         | YES  |     | NULL    |                |
| sumber_pendanaan | varchar(45)  | YES  |     | NULL    |                |
| biaya            | double       | YES  |     | NULL    |                |
| mulai_semester   | int(11)      | YES  |     | NULL    |                |
| akhir_semester   | int(11)      | YES  |     | NULL    |                |
| deskripsi        | varchar(100) | YES  |     | NULL    |                |
| kategori_id      | int(11)      | NO   | MUL | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+
9 rows in set (0,03 sec)


    */

    //panggil file yang berisi oeperasi db
    require_once 'db/class_risetd.php';
    //buat variabel untuk melakukan operasi db
    $obj = new Risetd();
    //buat variabel untuk mengambil data dari form dan menyimpannya dlm array
    $_dosen_id = $_POST['dosen_id'];
    $_judul = $_POST['judul'];
    $_sumber_pendanaan = $_POST['sumber_pendanaan'];
    $_biaya = $_POST['biaya'];
    $_mulai_semester = $_POST['mulai_semester'];
    $_akhir_semester = $_POST['akhir_semester'];
    $_deskripsi = $_POST['deskripsi'];      
    $_kategori_id = $_POST['kategori_id'];


    $_proses = $_POST['proses'];

    $ar_data[] = $_dosen_id ;
    $ar_data[] = $_judul;
    $ar_data[] = $_sumber_pendanaan ;
    $ar_data[] = $_biaya ;
    $ar_data[] = $_mulai_semester ;
    $ar_data[] = $_akhir_semester ;
    $ar_data[] = $_deskripsi   ;
    $ar_data[] = $_kategori_id ;
    //buat operasi jika memilih button simpan, update atau hapus
    $row = 0;
    if($_proses == "Simpan"){
        $row = $obj->simpan($ar_data);
    }elseif($_proses == "Update"){
        $_idedit = $_POST['idedit'];
        $ar_data[] = $_idedit;
        $row = $obj->ubah($ar_data);
    }elseif($_proses == "Hapus"){
        unset($ar_data);
        $_idedit = $_POST['idedit'];
        $row = $obj->hapus($_idedit);
    }
    //handeler jika gagal atau sukses
    if($row==0){
        echo "Gagal Proses";
    }else{
        //echo "Proses Sukses";
        //langsung direct ke daftar_kegiatan.php
        header('Location:risetdosen.php');
    }
?>