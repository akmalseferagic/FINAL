<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AkRa 47</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
    	<link href="css/style.css" rel="stylesheet">
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/jquery.validate.js"></script>
	
	<link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.min.css"/>

	<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>

  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-custom">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">Akra 47</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php">Home</a></li>
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Dosen
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="dosen.php">Tabel Dosen</a>
                            <br>
                          <a class="dropdown-item" href="grafik_dosen.php">Grafik Dosen</a>
                          <div class="dropdown-divider"></div>
                        </div>
                    </li>
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Program Studi
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="prodi.php">Tabel Program Studi</a>
                            <br>
                          <a class="dropdown-item" href="grafik_prodi.php">Grafik Program Studi</a>
                          <div class="dropdown-divider"></div>
                        </div>
                    </li>
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Rombel
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="rombel.php">Tabel Rombel</a>
                            <br>
                          <a class="dropdown-item" href="grafik_rombel.php">Grafik Rombel</a>
                          <div class="dropdown-divider"></div>
                        </div>
                    </li>
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Riset
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="risetdosen.php">Riset Dosen</a>
                            <br>
                          <a class="dropdown-item" href="kategoririset.php">Kategori Riset</a>
                          <div class="dropdown-divider"></div>
                        </div>
                      </li>
                    
                </ul>
            </div>
        </nav>
		</div>
	</div>
