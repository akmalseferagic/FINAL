<?php
    include_once 'top.php';
    require_once 'db/class_dosen.php';
    $obj_kegiatan = new Dosen();
    $_idedit = $_GET['id'];
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>

<script src="js/form_validasi_dosen.js"></script>

<form class="form-horizontal" method="POST" name="form_dosen" action="proses_dosen.php">
<fieldset>

<legend>Form Input Identitas Dosen</legend>

<div class="marginTop">
    <div class="form-group">
      <label class="col-md-4 control-label" for="nidn">NIDN</label>  
      <div class="col-md-4">
      <input id="nidn" name="nidn" type="text" placeholder="Masukkan NIDN" class="form-control input-md" value="<?php echo $data['nidn']?>">   
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="nama">Nama</label>  
      <div class="col-md-4">
      <input id="nama" name="nama" type="text" placeholder="Masukkan Nama" class="form-control input-md" value="<?php echo $data['nama']?>" >  
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="gelar_depan">Gelar Depan</label>  
      <div class="col-md-4">
      <input id="gelar_depan" name="gelar_depan" type="text" placeholder="Masukkan Gelar Depan" class="form-control input-md" value="<?php echo $data['gelar_depan']?>">    
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="gelar_belakang">Gelar Belakang</label>
      <div class="col-md-4">                     
        <textarea class="form-control" id="gelar_belakang" name="gelar_belakang"><?php echo $data['gelar_belakang']?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="tmp_lahir">Tempat Lahir</label>
      <div class="col-md-4">
      <input id="tmp_lahir" name="tmp_lahir" type="text" placeholder="Masukkan Tempat Lahir" class="form-control input-md" value="<?php echo $data['tmp_lahir']?>" >
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="jk">Jenis Kelamin</label>
      <div class="col-md-4">
      <input id="jk" name="jk" type="text" placeholder="Masukkan Jenis Kelamin" class="form-control input-md" value="<?php echo $data['jk']?>" >
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="prodi_id">Program Studi</label>
      <div class="col-md-4">
        <select id="prodi_id" name="prodi_id" class="form-control">
          <option value="0">Pilih Program Studi Anda</option>
          <option value="1">1. SI</option>
          <option value="2">2. TI</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="email">E-Mail</label>
      <div class="col-md-4">
      <input id="email" name="email" type="text" placeholder="Masukkan EMail" class="form-control input-md" value="<?php echo $data['email']?>" >
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="jabatan_id">Jabatan</label>
      <div class="col-md-4">
        <select id="jabatan_id" name="jabatan_id" class="form-control">
          <option value="0">Pilih Jabatan Anda</option>
          <option value="1">1. Pengajar</option>
          <option value="2">2. Asisten ahli</option>
          <option value="3">3. Lektor Ahli</option>
          <option value="4">4. Professor</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="pend_akhir">Pendidikan Akhir</label>
      <div class="col-md-4">
      <input id="pend_akhir" name="pend_akhir" type="text" placeholder="Masukkan Pendidikan Akhir" class="form-control input-md" value="<?php echo $data['pend_akhir']?>" >
      </div>
    </div>


    <div class="form-group">
      <label class="col-md-4 control-label" for="proses"></label>
      <div class="col-md-8">
      <?php
        if(empty($_idedit)){
        ?>
          <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
        <?php
        }else{
          ?>
          <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
          <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
          <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
        <?php
        }?>
      </div>
    </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
